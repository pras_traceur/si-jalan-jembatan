
<!-- CONTEN -->          
<!-- / .main-navbar -->
<div class="main-content-container container-fluid px-4">
<!-- Page Header -->
<div class="page-header row no-gutters py-4">
    <div class="col-lg-12 col-sm-4 text-center text-sm-left mb-0">
    <span class="text-uppercase page-subtitle">Dashboard</span>
    <h3 class="page-title">Aplikasi Data Base Jembatan Kabupaten Kotawaringin Barat</h3>
    </div>
</div>
<!-- End Page Header -->
<!-- Small Stats Blocks -->
<div class="row">
    <div class="col-lg col-md-6 col-sm-6 mb-4">
    <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
        <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
            <span class="stats-small__label text-uppercase">Jalan</span>
            <h6 class="stats-small__value count my-3">2,390</h6>
            </div>
            <div class="stats-small__data">
            <span class="stats-small__percentage stats-small__percentage--increase">4.7%</span>
            </div>
        </div>
        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
        </div>
    </div>
    </div>
    <div class="col-lg col-md-6 col-sm-6 mb-4">
    <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
        <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
            <span class="stats-small__label text-uppercase">Jembatan</span>
            <h6 class="stats-small__value count my-3">182</h6>
            </div>
            <div class="stats-small__data">
            <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
            </div>
        </div>
        <canvas height="120" class="blog-overview-stats-small-2"></canvas>
        </div>
    </div>
    </div>
    <div class="col-lg col-md-4 col-sm-6 mb-4">
    <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
        <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
            <span class="stats-small__label text-uppercase">Keluhan</span>
            <h6 class="stats-small__value count my-3">147</h6>
            </div>
            <div class="stats-small__data">
            <span class="stats-small__percentage stats-small__percentage--decrease">3.8%</span>
            </div>
        </div>
        <canvas height="120" class="blog-overview-stats-small-3"></canvas>
        </div>
    </div>
    </div>
    <div class="col-lg col-md-4 col-sm-6 mb-4">
    <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
        <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
            <span class="stats-small__label text-uppercase">Pengguna</span>
            <h6 class="stats-small__value count my-3">2,413</h6>
            </div>
            <div class="stats-small__data">
            <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
            </div>
        </div>
        <canvas height="120" class="blog-overview-stats-small-4"></canvas>
        </div>
    </div>
    </div>
    <div class="col-lg col-md-4 col-sm-12 mb-4">
    <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
        <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center">
            <span class="stats-small__label text-uppercase">Berita</span>
            <h6 class="stats-small__value count my-3">1,281</h6>
            </div>
            <div class="stats-small__data">
            <span class="stats-small__percentage stats-small__percentage--decrease">2.4%</span>
            </div>
        </div>
        <canvas height="120" class="blog-overview-stats-small-5"></canvas>
        </div>
    </div>
    </div>
</div>
<!-- End Small Stats Blocks -->
<div class="row">
    <!-- Users Stats -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
        <div class="card-header border-bottom">
            <h6 class="m-0">Peta Daerah</h6>
        </div>
        <div class="card-body pt-0">
            <div class="row border-bottom py-2 bg-light">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1020379.338270457!2d111.1985963475852!3d-2.563442816043734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e088d657f31bfc7%3A0x98c967d3b2f9a201!2sKabupaten%20Kotawaringin%20Barat%2C%20Kalimantan%20Tengah!5e0!3m2!1sid!2sid!4v1569986348155!5m2!1sid!2sid" width="100%"; height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>
        </div>
    </div>
    <!-- End Users Stats -->
</div>

<div class="row">
    <!-- Discussions Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
        <div class="card card-small blog-comments">
        <div class="card-header border-bottom">
            <h6 class="m-0">Laporan Keluhan</h6>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-comments__item d-flex p-3">
                    <div class="blog-comments__avatar mr-3">
                    <img src="images/avatars/1.jpg" alt="User avatar" /> </div>
                    <div class="blog-comments__content">
                        
                        <div class="blog-comments__meta text-muted">
                            <a class="text-secondary" href="#">Galang</a> on
                            <a class="text-secondary" href="#">Jembatan Kotawaringin Lama</a>
                            <span class="text-muted">– 1 hari lalu</span>
                        </div>

                        <p class="m-0 my-1 mb-2 text-muted">Saya mau melaporkan bahwa di Jembatan Kotawaringin Lama kondisinya sudah ambles dan tidak bisa di lewati oleh Masyarakat...</p>

                        <div class="blog-comments__actions">
                            <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-white">
                                <span class="text-success">
                                <i class="material-icons">check</i>
                                </span> Approve </button>
                            <button type="button" class="btn btn-white">
                                <span class="text-danger">
                                <i class="material-icons">clear</i>
                                </span> Reject </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-lg-4">
                        <div class="row" style="transform: translate(-10%,10%);">
                        <div class="col-6">
                            <div class="card card-small card-post card-post--1">
                                <div class="card-post__image-custom" style="background-image: url('images/foto/foto1.jpg');"></div> 
                            </div> 
                        </div>
                        <div class="col-6">
                            <div class="card card-small card-post card-post--1">
                                <div class="card-post__image-custom" style="background-image: url('images/foto/foto2.jpg');"></div> 
                            </div> 
                        </div>
                        </div>
                    </div>

            </div>


            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-comments__item d-flex p-3">
                    <div class="blog-comments__avatar mr-3">
                    <img src="images/avatars/3.jpg" alt="User avatar" /> </div>
                    <div class="blog-comments__content">
                        
                        <div class="blog-comments__meta text-muted">
                            <a class="text-secondary" href="#">Suradi</a> on
                            <a class="text-secondary" href="#">Kelurahan/Desa Pangkalan Tiga </a>
                            <span class="text-muted">– 2 hari lalu</span>
                        </div>

                        <p class="m-0 my-1 mb-2 text-muted">Saya mau melaporkan bahwa di Jalan di Kelurahan/Desa Pangkalan Tiga kondisinya rusak parah tidak bisa di lewati...</p>

                        <div class="blog-comments__actions">
                            <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-white">
                                <span class="text-success">
                                <i class="material-icons">check</i>
                                </span> Approve </button>
                            <button type="button" class="btn btn-white">
                                <span class="text-danger">
                                <i class="material-icons">clear</i>
                                </span> Reject </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-lg-4">
                        <div class="row" style="transform: translate(-10%,10%);">
                        <div class="col-6">
                            <div class="card card-small card-post card-post--1">
                                <div class="card-post__image-custom" style="background-image: url('images/foto/foto5.jpg');"></div> 
                            </div> 
                        </div>
                        <div class="col-6">
                        </div>
                        </div>
                    </div>

            </div>


            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-comments__item d-flex p-3">
                    <div class="blog-comments__avatar mr-3">
                    <img src="images/avatars/4.jpg" alt="User avatar" /> </div>
                    <div class="blog-comments__content">
                        
                        <div class="blog-comments__meta text-muted">
                            <a class="text-secondary" href="#">Nur</a> on
                            <a class="text-secondary" href="#">Kelurahan/Desa Pandu Sanjaya</a>
                            <span class="text-muted">– 1 hari lalu</span>
                        </div>

                        <p class="m-0 my-1 mb-2 text-muted">Saya mau melaporkan bahwa di Kelurahan/Desa Pandu Sanjaya kondisinya sudah ambles dan tidak bisa di lewati oleh Masyarakat...</p>

                        <div class="blog-comments__actions">
                            <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-white">
                                <span class="text-success">
                                <i class="material-icons">check</i>
                                </span> Approve </button>
                            <button type="button" class="btn btn-white">
                                <span class="text-danger">
                                <i class="material-icons">clear</i>
                                </span> Reject </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-lg-4">
                        <div class="row" style="transform: translate(-10%,10%);">
                        <div class="col-6">
                            <div class="card card-small card-post card-post--1">
                                <div class="card-post__image-custom" style="background-image: url('images/foto/foto3.jpg');"></div> 
                            </div> 
                        </div>
                        <div class="col-6">
                            <div class="card card-small card-post card-post--1">
                                <div class="card-post__image-custom" style="background-image: url('images/foto/foto4.jpg');"></div> 
                            </div> 
                        </div>
                        </div>
                    </div>

            </div>

        </div>
        <div class="card-footer border-top">
            <div class="row">
            <div class="col text-center view-report">
                <button type="submit" class="btn btn-white">Lihat Semua Laporan</button>
            </div>
            </div>
        </div>
        </div>
    </div>
    <!-- End Discussions Component -->
</div>

<div class="row">
    <!-- Users Stats -->
    <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
    <div class="card card-small">
        <div class="card-header border-bottom">
        <h6 class="m-0">Pengguna</h6>
        </div>
        <div class="card-body pt-0">
        <div class="row border-bottom py-2 bg-light">
            <div class="col-12 col-sm-6">
            <div id="blog-overview-date-range" class="input-daterange input-group input-group-sm my-auto ml-auto mr-auto ml-sm-auto mr-sm-0" style="max-width: 350px;">
                <input type="text" class="input-sm form-control" name="start" placeholder="Start Date" id="blog-overview-date-range-1">
                <input type="text" class="input-sm form-control" name="end" placeholder="End Date" id="blog-overview-date-range-2">
                <span class="input-group-append">
                <span class="input-group-text">
                    <i class="material-icons"></i>
                </span>
                </span>
            </div>
            </div>
            <div class="col-12 col-sm-6 d-flex mb-2 mb-sm-0">
            <button type="button" class="btn btn-sm btn-white ml-auto mr-auto ml-sm-auto mr-sm-0 mt-3 mt-sm-0">Laporan &rarr;</button>
            </div>
        </div>
        <canvas height="130" style="max-width: 100% !important;" class="blog-overview-users"></canvas>
        </div>
    </div>
    </div>
    <!-- End Users Stats -->
    <!-- Users By Device Stats -->
    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
    <div class="card card-small h-100">
        <div class="card-header border-bottom">
        <h6 class="m-0">Akses Pengguna</h6>
        </div>
        <div class="card-body d-flex py-0">
        <canvas height="220" class="blog-users-by-device m-auto"></canvas>
        </div>
        <div class="card-footer border-top">
        <div class="row">
            <div class="col">
            <select class="custom-select custom-select-sm" style="max-width: 130px;">
                <option selected>Minggu Terakhir</option>
                <option value="1">Hari Ini</option>
                <option value="2">Bulan Lalu</option>
                <option value="3">Tahun Lalu</option>
            </select>
            </div>
            <div class="col text-right view-report">
            <a href="#">Laporan &rarr;</a>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- End Users By Device Stats -->
    
</div>
</div>
<!-- END CONTEN -->
