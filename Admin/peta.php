<?php include 'header.php'; ?>

<!-- CONTEN -->          
<!-- / .main-navbar -->
<div class="main-content-container container-fluid px-4">

<!-- Page Header -->
<div class="page-header row no-gutters py-4">
    <div class="col-lg-12 col-sm-4 text-center text-sm-left mb-0">
    </div>
</div>
<!-- End Page Header -->
<!-- Small Stats Blocks -->

    <div class="row">
        <!-- Users Stats -->
        <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Peta Jalan</h6>
            </div>
            <div class="card-body pt-0">
                <div class="row border-bottom py-2 bg-light">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3985.4114510303016!2d111.63213711475615!3d-2.693184698037915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e08ee4cc5ec1063%3A0x48de693dabb80341!2sDinas%20PUPR%20Kotawaringin%20Barat!5e0!3m2!1sid!2sid!4v1570041680974!5m2!1sid!2sid" width="100%"; height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            </div>
        </div>
        <!-- End Users Stats -->
    </div>

    <div class="row">
        <!-- Users Stats -->
        <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Peta Jembatan</h6>
            </div>
            <div class="card-body pt-0">
                <div class="row border-bottom py-2 bg-light">
                    <iframe src="https://www.google.com/maps/d/embed?mid=1gnj50JKvFas7ep3-o0Oh6aVvCmqt7Rqq" width="100%"; height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            </div>
        </div>
        <!-- End Users Stats -->
    </div>

</div>
<?php include 'footer.php'; ?>